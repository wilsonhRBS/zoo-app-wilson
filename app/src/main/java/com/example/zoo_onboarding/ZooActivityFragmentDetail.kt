package com.example.zoo_onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.zoo_onboarding.databinding.ZooActivityFragmentDetailBinding

class ZooActivityFragmentDetail: Fragment() {
    private var _binding: ZooActivityFragmentDetailBinding? = null
    private val binding: ZooActivityFragmentDetailBinding get() = _binding!!

    private val args by navArgs<ZooActivityFragmentDetailArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bndng = ZooActivityFragmentDetailBinding.inflate(inflater,container,false)
        _binding = bndng
        return bndng.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        displayAnimal()
    }

    private fun initListeners() = with(binding) {
        btnHome.setOnClickListener{
            val action = ZooActivityFragmentDetailDirections.
                actionZooActivityFragmentDetailToZooActivityFragmentHome(args.zooName)
            findNavController().navigate(action)
        }
    }

    private fun displayAnimal() = with(binding) {
        tvDesc.text = args.animalDesc
        tvName.text = args.animalName
        ivAnimal.setImageResource(args.animalImg)
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}