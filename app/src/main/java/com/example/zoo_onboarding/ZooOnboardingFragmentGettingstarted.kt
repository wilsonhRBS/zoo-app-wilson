package com.example.zoo_onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zoo_onboarding.databinding.ZooOnboardingFragmentGettingstartedBinding

class ZooOnboardingFragmentGettingstarted: Fragment() {
    private var _binding: ZooOnboardingFragmentGettingstartedBinding? = null
    private val binding: ZooOnboardingFragmentGettingstartedBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bndng = ZooOnboardingFragmentGettingstartedBinding.inflate(inflater,container,false)
        _binding = bndng
        return bndng.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding) {
        btnGsfragBack.setOnClickListener {
            val action = ZooOnboardingFragmentGettingstartedDirections.
                actionZooOnboardingFragmentGettingstartedToZooOnboardingFragmentHello()
            findNavController().navigate(action)
        }
        btnGsfragNext.setOnClickListener {
            val action = ZooOnboardingFragmentGettingstartedDirections.
                actionZooOnboardingFragmentGettingstartedToZooOnboardingFragmentFinish()
            findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}