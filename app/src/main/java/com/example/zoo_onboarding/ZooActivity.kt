package com.example.zoo_onboarding

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.example.zoo_onboarding.databinding.ZooActivityBinding

class ZooActivity: AppCompatActivity() {
    private lateinit var binding: ZooActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ZooActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViews()
    }

    private fun initViews() = with(binding) {
        val nc = findNavController(R.id.nav_host_fragment)
        nc.navigate(R.id.zooActivityFragmentHome, intent.extras)
    }
}