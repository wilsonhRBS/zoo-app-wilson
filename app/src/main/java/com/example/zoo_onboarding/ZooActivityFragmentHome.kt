package com.example.zoo_onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.zoo_onboarding.databinding.ZooActivityFragmentHomeBinding

class ZooActivityFragmentHome: Fragment() {
    private var _binding: ZooActivityFragmentHomeBinding? = null
    private val binding: ZooActivityFragmentHomeBinding get() = _binding!!
    private val args by navArgs<ZooActivityFragmentHomeArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bndng = ZooActivityFragmentHomeBinding.inflate(inflater,container,false)
        _binding = bndng
        return bndng.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding) {
        //Welcome message + toast
        tvHome.text = getString(R.string.WelcomeToThe, args.zooName)
        //tvHome.text = getString(R.string.WelcomeToThe, (activity as ZooActivity).zooName)
        //getString(R.string.WelcomeToThe, (activity as ZooActivity).zooName).toast(requireContext())
        //init buttons
        btnSelect1.setOnClickListener{
            navigateToDetail(getString(R.string.animal_name_1),
                getString(R.string.animal_desc_1),
                R.drawable.rhino)
        }
        btnSelect2.setOnClickListener{
            navigateToDetail(getString(R.string.animal_name_2),
                getString(R.string.animal_desc_2),
                R.drawable.giraffe)
        }
        btnSelect3.setOnClickListener{
            navigateToDetail(getString(R.string.animal_name_3),
                getString(R.string.animal_desc_3),
                R.drawable.leopard)
        }
        btnSelect4.setOnClickListener{
            navigateToDetail(getString(R.string.animal_name_4),
                getString(R.string.animal_desc_4),
                R.drawable.lion)
        }
    }

    private fun navigateToDetail(animal_name: String, animal_desc: String, animal_img_ID: Int) {
        val action = ZooActivityFragmentHomeDirections.
            actionZooActivityFragmentHomeToZooActivityFragmentDetail(
            animal_img_ID, animal_name, animal_desc, args.zooName)
        findNavController().navigate(action)
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}