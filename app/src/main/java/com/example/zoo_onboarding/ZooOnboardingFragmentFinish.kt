package com.example.zoo_onboarding

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zoo_onboarding.databinding.ZooOnboardingFragmentFinishBinding

class ZooOnboardingFragmentFinish: Fragment() {
    private var _binding: ZooOnboardingFragmentFinishBinding? = null
    private val binding: ZooOnboardingFragmentFinishBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bndng = ZooOnboardingFragmentFinishBinding.inflate(inflater,container,false)
        _binding = bndng
        return bndng.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding) {
        btnFinishfragFinish.setOnClickListener {
            /*val action = ZooOnboardingFragmentFinishDirections.
                actionZooOnboardingFragmentFinishToZooActivity(getString(R.string.zoo_name))
            findNavController().navigate(action)*/
            val intent = Intent(activity, ZooActivity::class.java)
            intent.putExtra("zoo_name", getString(R.string.zoo_name))
            startActivity(intent)
        }
        btnFinishfragBack.setOnClickListener {
            val action = ZooOnboardingFragmentFinishDirections.
                actionZooOnboardingFragmentFinishToZooOnboardingFragmentGettingstarted()
            findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}