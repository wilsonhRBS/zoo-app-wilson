package com.example.zoo_onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zoo_onboarding.databinding.ZooOnboardingFragmentHelloBinding

class ZooOnboardingFragmentHello: Fragment() {
    private var _binding: ZooOnboardingFragmentHelloBinding? = null
    private val binding: ZooOnboardingFragmentHelloBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bndng = ZooOnboardingFragmentHelloBinding.inflate(inflater,container,false)
        _binding = bndng
        return bndng.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding) {
        btnHellofragNext.setOnClickListener {
            val action = ZooOnboardingFragmentHelloDirections.
                actionZooOnboardingFragmentHelloToZooOnboardingFragmentGettingstarted()
            findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}